const express = require('express')
const bodyParser = require('body-parser');
const cors       = require('cors');
const path       = require('path');
const dotenv = require('dotenv').config({path:'.env'});
const app = express()
const PORT = process.env.PORT || 5000

const jsforce = require('jsforce')

app.get('/', (req, res) => res.send('Home!'))

app.use(cors());
app.use(bodyParser.json());

app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`))

app.post("/login", function(req, res) {
  req.app.use(bodyParser.json());
  if(req.body.username && req.body.password) {

      var username = req.body.username;
      var password = req.body.password;

      var conn = new jsforce.Connection({
        oauth2: {
          loginUrl: 'https://atco--CCTest.cs24.my.salesforce.com',
          clientId: process.env.MYCLIENTKEY,
          clientSecret: process.env.MYCLIENTPASSWORD,
          redirectUri: 'http://localhost:5000/'
        }
      })
      conn.login(username, password, function (err, userInfo) {
        if (err) {
          return console.error(err)
        }
        res.json(conn.accessToken);
      })
  }
});
